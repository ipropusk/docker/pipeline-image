FROM debian:buster

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get --yes --quiet --no-install-recommends install \
      apt-utils && \
    apt-get --yes upgrade && \
    apt-get --yes --quiet --no-install-recommends install \
      ca-certificates \
      wget \
      apt-transport-https \
      gnupg2 \
      curl \
      jq \
      git \
      tree && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
    echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list && \
    wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_10/Release.key -O- | apt-key add - && \
    curl https://baltocdn.com/helm/signing.asc | apt-key add - && \
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list && \
    apt-get update && \
    apt-get --yes --quiet --no-install-recommends install \
      pass \
      kubectl \
      helm \
      skopeo \
      buildah && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
